import React from 'react'
import { View,
         Text,
         StyleSheet,
         TouchableWithoutFeedback,
         TouchableOpacity, 
        } from 'react-native'
       
import commonStyles from '../commonStyles';
import Swipeable  from 'react-native-gesture-handler/Swipeable';
import Icon from 'react-native-vector-icons/FontAwesome'
import moment from 'moment';
import 'moment/locale/pt-br'

export default props => {

    const doneOrNotStyle = props.doneAt != null ? 
        { textDecorationLine: 'line-through' } : {}

    const date = props.doneAt ? props.doneAt : props.estimateAt
    const formatteDate = moment(date).locale('pt-bt')
        .format('ddd, D [de] MMM')

    const getRigthContent = () => {
        return (
            <TouchableOpacity style={styles.rigth}
                 onPress={() => props.onDelete && props.onDelete(props.id)}>
                <Icon name='trash' size={30} color="#FFF"/>
            </TouchableOpacity>
        )
    }

    const getLeftContent = () => {
        return(
            <View style={styles.left}>
                <Icon name='trash' size={20} color='#FFF'
                 style={styles.excludeIcon}/>
                <Text style={styles.excludeText}>Excluir</Text>
            </View>
        )
    }
    return (
        <Swipeable
            renderRightActions={getRigthContent}
            renderLeftActions={getLeftContent}
            onSwipeableLeftOpen={() => props.onDelete && props.onDelete(props.id)}>
            <View style={styles.container}>
                <TouchableWithoutFeedback
                    onPress={() => props.toggleTask(props.id)}>
                <View style={styles.checkContainer}> 
                    {getCheckView(props.doneAt)} 
                </View>
                </TouchableWithoutFeedback>
                <View>
                    <Text style={[styles.desc, doneOrNotStyle]}>{props.desc}</Text>
                    <Text style={styles.date}>{formatteDate}</Text>
                </View>                     
            </View>
        </Swipeable>
    )
}

function getCheckView(doneAt){
    if(doneAt != null){
        return (
            <View style={styles.done}>
                 <Icon name='check' size={20} color='#FFF'></Icon>
            </View>
        )
    } else {
        return (
            <View style={styles.pending}>
               
            </View>
        )
    }
    
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderColor: '#AAA',
        borderBottomWidth: 2,
        alignItems: 'center',
        paddingVertical: 10,
        backgroundColor: '#FFF'
    },
    checkContainer: {
        width: '20%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    pending: {
        height: 25,
        width: 25,
        borderRadius: 13,
        borderWidth: 1,
        borderColor: '#555'
    },
    done: {
        width: 25,
        height: 25,
        borderRadius: 13,
        backgroundColor: '#4D7031',
        alignItems: 'center',
        justifyContent: 'center'

    },
    desc: {
        color: commonStyles.colors.mainText,
        fontSize: 15
    },
    date: {
        color: commonStyles.colors.subText, 
        fontSize: 12
    },
    rigth: {
        backgroundColor: 'red',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingHorizontal: 20
    },
    left: {
        backgroundColor: 'red',
        flexDirection:'row',
        alignItems: 'center',
        flex: 1
    },
    excludeText: {
        color:'#FFF',
        fontSize: 20,
        margin: 10
    },
    excludeIcon: {
        marginLeft: 10
    }
})