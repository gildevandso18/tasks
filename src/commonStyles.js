export default {
    fontFamily: 'Lato',
    colors: {
        secondary: '#FFF',
        today: '#B13B44',
        mainText: '#222',
        subText: '#555'
    }
}