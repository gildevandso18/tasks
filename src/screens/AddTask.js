import React, {Component} from 'react'
import {Modal,
        View,
        Text,
        TouchableOpacity,
        TextInput,
        StyleSheet,
        Platform,
        TouchableWithoutFeedback,
        }
from 'react-native'
import commonStyles from '../commonStyles'
import moment from 'moment'
import DateTimePicker from '@react-native-community/datetimepicker'

const initialState = { desc: '', date: new Date(), showdDatePicker: false }
export default class AddTask extends Component {
    
    state = {
        ...initialState
    }

    save = () => {
        const newTask = {
            desc: this.state.desc, 
            date: this.state.date
        }
        
        this.props.onSave =  this.props.onSave(newTask)
        this.setState({ ...initialState })
        
    }

    getDateTimePicker = () => {
        let datePicker = <DateTimePicker
        value={this.state.date}
        onChange={(_, date) => this.setState({ date, showdDatePicker: false })}
        mode='date'/>

        const dateString = moment(this.state.date).format('dddd, D [de] MMMM [de] YYYY')

        if(Platform.OS === 'android') {
            datePicker = (
                <View>
                    <TouchableOpacity onPress={() => this.setState({ showdDatePicker: true })}>
                        <Text style={styles.date}>
                            {dateString}
                        </Text>
                    </TouchableOpacity>
                    {this.state.showdDatePicker && datePicker}
                </View>
            )
        }

        return datePicker
         
    }
    render(){
        return (
            <Modal transparent={true} visible={this.props.isVisible}
                onRequestClose={this.props.onCancel}
                animationType='slide'>
                <TouchableWithoutFeedback 
                    onPress={this.props.onCancel}>
                    <View style={styles.background}></View>
                </TouchableWithoutFeedback>
                <View style={styles.container}>
                    <Text style={styles.hearder}>Nova Tarefa</Text>
                    <TextInput style={styles.input}
                        placeholder="Informe a Descrição..."
                        paddingLeft={5}
                        onChangeText={desc => this.setState({ desc })}
                        value={this.state.desc}/>

                    {this.getDateTimePicker()}

                    <View style={styles.buttons}>
                        <TouchableOpacity onPress={this.props.onCancel}>
                            <Text style={styles.button}>Calcelar</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.save}>
                            <Text style={styles.button}>Salvar</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                 <TouchableWithoutFeedback 
                    onPress={this.props.onCancel}>
                    <View style={styles.background}></View>
                </TouchableWithoutFeedback>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.6)'

    },
    container: {
        backgroundColor: '#FFF'
    },
    hearder: {
        backgroundColor: commonStyles.colors.today,
        color: commonStyles.colors.secondary,
        textAlign: 'center',
        padding: 15,
        fontSize: 18
    },
    input: {
        height: 40,
        margin: 15,
        backgroundColor: '#FFF',
        borderWidth: 1,
        borderColor: '#E3E3E3',
        borderRadius: 6
    },
    buttons: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    button: {
        margin: 20,
        marginRight: 30,
        fontSize: 20,
        color: commonStyles.colors.today
    },
    date: {
        fontSize: 20,
        marginLeft: 17,
    }
})
